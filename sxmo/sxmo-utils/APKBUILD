# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-utils
pkgver=1.3.2
pkgrel=0
pkgdesc="Utility scripts, programs, and configs that hold the Sxmo UI environment together"
url="https://git.sr.ht/~mil/sxmo-utils"
arch="all"
license="MIT"
makedepends="libx11-dev xproto linux-headers"
depends="
	sxmo-dmenu
	sxmo-dwm
	sxmo-st
	sxmo-surf
	svkbd
	lisgd

	alsa-utils
	autocutsel
	codemadness-frontends
	conky
	coreutils
	curl
	dunst
	ffmpeg
	gawk
	geoclue
	grep
	inotify-tools
	mediainfo
	modemmanager
	mpv
	ncurses
	sfeed
	sxiv
	terminus-font
	font-terminus-nerd
	tzdata
	v4l-utils
	vis
	w3m
	xclip
	xdotool
	xdpyinfo
	xinput
	xprop
	xrandr
	xrdb
	xsel
	xset
	xsetroot
	xwininfo
	youtube-dl
"

options="!check" # has no tests
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-utils/archive/$pkgver.tar.gz"
install="$pkgname.post-install $pkgname.pre-deinstall"

package() {
	mkdir -p "$pkgdir/etc/modules-load.d/"
	printf %b "snd-aloop" > "$pkgdir/etc/modules-load.d/sxmo.conf"
	mkdir -p "$pkgdir/etc/modprobe.d/"
	printf %b "options snd slots=,snd-aloop" > "$pkgdir/etc/modprobe.d/sxmo.conf"

	make  -C "$builddir" DESTDIR=$pkgdir install
}

sha512sums="f9588a54774e9698381f10718e62c134786179a6f8b1d71b6b43bd81d76a8ed4fa7c6ae5b767d9edacafcac9a2dfabda794459875cdb3341290ae50a0e929aee  sxmo-utils-1.3.2.tar.gz"
