# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=peruse
pkgver=0_git20210124
pkgrel=0
_commit="341c513a59df0aee73a29d035a7a4bc6ae4b102b"
pkgdesc="A comic book viewer based on Frameworks 5, for use on multiple form factors"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/graphics/peruse"
license="LGPL-2.1-or-later AND LicenseRef-KDE-Accepted-LGPL"
depends="
	kirigami2
	qt5-qtbase-sqlite
	qt5-qtimageformats
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kfilemetadata-dev
	knewstuff-dev
	qt5-qtdeclarative-dev
	"
source="https://invent.kde.org/graphics/peruse/-/archive/$_commit/peruse-$_commit.tar.gz"
subpackages="$pkgname-creator"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

creator() {
	pkgdesc="Creation tool for comic books"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share/metainfo
	mv "$pkgdir"/usr/bin/perusecreator "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/share/metainfo/org.kde.perusecreator.appdata.xml \
		"$subpkgdir"/usr/share/metainfo
}

sha512sums="b9981cb63e793e0f5053f03a2de798d71d4b350a4243223dbbfc8af27def4b18a1cab1f6fd17eb3b07639af4962864de533df50b2722e2c8a6796295912801b9  peruse-341c513a59df0aee73a29d035a7a4bc6ae4b102b.tar.gz"
